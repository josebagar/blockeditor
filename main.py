#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#    This file is part of My Text Editor.
#
#    My Text Editor is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    My Text Editor is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with My Text Editor.  If not, see <http://www.gnu.org/licenses/>.

# Copyright (C) 2013 Joseba García Etxebarria <joseba.gar@gmail.com>. All rights reserved.         

"""This is the main program file for my Nastran editor"""

import os,sys

# Import Qt modules
from PyQt4 import QtCore,QtGui

# Other dialogs
from dialog_main import dialog_main

def main():
    # Again, this is boilerplate, it's going to be the same on 
    # almost every app you write
    app = QtGui.QApplication(sys.argv)
    window=dialog_main()
    window.show()

    # It's exec_ because exec is a reserved word in Python
    sys.exit(app.exec_())
    

if __name__ == "__main__":
    main()
