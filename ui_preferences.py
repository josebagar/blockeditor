# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_preferences.ui'
#
# Created: Mon Oct 28 19:55:02 2013
#      by: PyQt4 UI code generator 4.10.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Preferences(object):
    def setupUi(self, Preferences):
        Preferences.setObjectName(_fromUtf8("Preferences"))
        Preferences.resize(472, 182)
        self.layoutWidget = QtGui.QWidget(Preferences)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 451, 161))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.labelNastran = QtGui.QLabel(self.layoutWidget)
        self.labelNastran.setObjectName(_fromUtf8("labelNastran"))
        self.horizontalLayout_5.addWidget(self.labelNastran)
        self.lineNastran = QtGui.QLineEdit(self.layoutWidget)
        self.lineNastran.setEnabled(False)
        self.lineNastran.setObjectName(_fromUtf8("lineNastran"))
        self.horizontalLayout_5.addWidget(self.lineNastran)
        self.button_NastranPath = QtGui.QPushButton(self.layoutWidget)
        self.button_NastranPath.setObjectName(_fromUtf8("button_NastranPath"))
        self.horizontalLayout_5.addWidget(self.button_NastranPath)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.labelNastranLibrary = QtGui.QLabel(self.layoutWidget)
        self.labelNastranLibrary.setObjectName(_fromUtf8("labelNastranLibrary"))
        self.horizontalLayout_2.addWidget(self.labelNastranLibrary)
        self.lineNastranLibrary = QtGui.QLineEdit(self.layoutWidget)
        self.lineNastranLibrary.setEnabled(False)
        self.lineNastranLibrary.setStatusTip(_fromUtf8(""))
        self.lineNastranLibrary.setObjectName(_fromUtf8("lineNastranLibrary"))
        self.horizontalLayout_2.addWidget(self.lineNastranLibrary)
        self.button_NastranLibraryPath = QtGui.QPushButton(self.layoutWidget)
        self.button_NastranLibraryPath.setObjectName(_fromUtf8("button_NastranLibraryPath"))
        self.horizontalLayout_2.addWidget(self.button_NastranLibraryPath)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.labelPatran = QtGui.QLabel(self.layoutWidget)
        self.labelPatran.setObjectName(_fromUtf8("labelPatran"))
        self.horizontalLayout.addWidget(self.labelPatran)
        self.linePatran = QtGui.QLineEdit(self.layoutWidget)
        self.linePatran.setEnabled(False)
        self.linePatran.setStatusTip(_fromUtf8(""))
        self.linePatran.setObjectName(_fromUtf8("linePatran"))
        self.horizontalLayout.addWidget(self.linePatran)
        self.button_PatranPath = QtGui.QPushButton(self.layoutWidget)
        self.button_PatranPath.setObjectName(_fromUtf8("button_PatranPath"))
        self.horizontalLayout.addWidget(self.button_PatranPath)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.buttonBox = QtGui.QDialogButtonBox(self.layoutWidget)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Preferences)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Preferences.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Preferences.reject)
        QtCore.QMetaObject.connectSlotsByName(Preferences)

    def retranslateUi(self, Preferences):
        Preferences.setWindowTitle(_translate("Preferences", "Preferences", None))
        self.labelNastran.setText(_translate("Preferences", "Nastran dir:", None))
        self.lineNastran.setPlaceholderText(_translate("Preferences", "Please choose path to Nastran", None))
        self.button_NastranPath.setText(_translate("Preferences", "Choose...", None))
        self.labelNastranLibrary.setText(_translate("Preferences", "Nastran library path", None))
        self.lineNastranLibrary.setPlaceholderText(_translate("Preferences", "[Optional] Please choose path to Nastran Library", None))
        self.button_NastranLibraryPath.setText(_translate("Preferences", "Choose...", None))
        self.labelPatran.setText(_translate("Preferences", "Patran dir:", None))
        self.linePatran.setPlaceholderText(_translate("Preferences", "[Optional] Please choose path to Patran", None))
        self.button_PatranPath.setText(_translate("Preferences", "Choose...", None))

