#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    This file is part of My Text Editor.
#
#    My Text Editor is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    My Text Editor is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with My Text Editor.  If not, see <http://www.gnu.org/licenses/>.

# Copyright (C) 2013 Joseba García Etxebarria <joseba.gar@gmail.com>. All rights reserved.

"""This is the Nastran card reader source code for my text editor."""

def getFields(line):
    blocks = []
    # Free field format
    if ',' in line:
        blocks = line.split(',')
        if len(blocks) > 10:
            blocks = blocks[0:10]

        return blocks, 'free'

    # Store the different cards
    blocks.insert(0, line[0:8].strip())
    field  = 1
    pos    = 8

    # Vars for short field notation
    width  = 8
    fields = 9
    
    # Vars for long field notation
    notation = 'short'
    if line[0:8].strip().endswith('*'):
        notation = 'long'
        width  = 16
        fields = 5

    while pos < 72:
        blocks.insert(field, line[pos:pos+width].strip())
        pos += width
        field += 1
    blocks.insert(field, line[pos:pos+8].strip())

    return (blocks, notation)