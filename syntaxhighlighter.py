#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    This file is part of My Text Editor.
#
#    My Text Editor is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    My Text Editor is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with My Text Editor.  If not, see <http://www.gnu.org/licenses/>.

# Copyright (C) 2013 Joseba García Etxebarria <joseba.gar@gmail.com>. All rights reserved.

"""This is the syntax highlighter source code for my text editor."""

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys
import cardreader

class highlighter( QSyntaxHighlighter ):
    def __init__( self, parent ):
        self.delimiters = ['CEND', 'BEGIN BULK', 'BEGIN SUPER', 'ENDDATA']
        self.nastranCards = ['ACCEL', 'ACCEL1', 'ACLOAD', 'ACMODL', 'ACSRCE', 'ADAPT', 'ADUMi', 'AECOMP',
                             'AECOMPL', 'AEDW', 'AEFACT', 'AEFORCE', 'AELINK', 'AELIST', 'AEPARM',
                             'AEPRESS', 'AERO', 'AEROS', 'AESTAT', 'AESURF', 'AESURFS', 'ASET', 'ASET1',
                             'AXIC', 'AXIF', 'AXSLOT', 'BAROR', 'BCBODY', 'BCBOX', 'BCHANGE', 'BCMATL',
                             'BCMOVE', 'BCONP', 'BCPARA', 'BCPROP', 'BCTABLE', 'BDYLIST', 'BDYOR',
                             'BEAMOR', 'BFRlC', 'BLSEG', 'BNDFIX', 'BNDFIX1', 'BNDFRE1', 'BNDFREE',
                             'BNDFREE1', 'BNDGRID', 'BOUTPUT', 'BSET', 'BSET1', 'BSURF', 'BWIDTH',
                             'CAABSF', 'CACINF3', 'CACINF4', 'CAERO1', 'CAERO2', 'CAERO3', 'CAERO4',
                             'CAERO5', 'CAXIFi', 'CBAR', 'CBARAO', 'CBEAM', 'CBEND', 'CBUSH',
                             'CBUSH1D', 'CBUTT', 'CCONEAX', 'CCRSFIL', 'CDAMP1', 'CDAMP1D', 'CDAMP2',
                             'CDAMP2D', 'CDAMP3', 'CDAMP4', 'CDAMP5', 'CDUMi', 'CELAS1', 'CELAS1D',
                             'CELAS2', 'CELAS2D', 'CELAS3', 'CELAS4', 'CFILLET', 'CFLUIDi', 'CGAP',
                             'CHACAB', 'CHACBR', 'CHBDYE', 'CHBDYG', 'CHBDYP', 'CHEXA', 'CLOAD',
                             'CMASS1', 'CMASS2', 'CMASS3', 'CMASS4', 'COMBWLD', 'CONM1', 'CONM2',
                             'CONROD', 'CONTRLT', 'CONV', 'CONVM', 'CORD1C', 'CORD1R', 'CORD1S',
                             'CORD2C', 'CORD2R', 'CORD2S', 'CORD3G', 'CPENTA', 'CQUAD', 'CQUAD4',
                             'CQUAD8', 'CQUADR', 'CQUADX', 'CRAC2D', 'CRAC3D', 'CREEP', 'CROD',
                             'CSET', 'CSET1', 'CSHEAR', 'CSLOT3', 'CSLOT4', 'CSPOT', 'CSSCHD', 'CSUPER',
                             'CSUPEXT', 'CTETRA', 'CTRIA3', 'CTRIA6', 'CTRIAR', 'CTRIAX', 'CTRIAX6',
                             'CTUBE', 'CVISC', 'CWELD', 'CYAX', 'CYJOIN', 'CYSUP', 'CYSYM', 'DAMPGBL',
                             'DAREA', 'DCONADD', 'DCONSTR', 'DDVAL', 'DEFORM', 'DEFUSET', 'DELAY',
                             'DEQATN', 'DESVAR', 'DIVERG', 'DLINK', 'DLOAD', 'DMI', 'DMIAX', 'DMIG',
                             'DMIJ', 'DMIJI', 'DMIK', 'DOPTPRM', 'DPHASE', 'DRESP1', 'DRESP2', 'DRESP3',
                             'DSCREEN', 'DTABLE', 'DTI', 'DVBSHAP', 'DVCREL1', 'DVCREL2', 'DVGRID',
                             'DVMREL1', 'DVMREL2', 'DVPREL1', 'DVPREL2', 'DVSHAP', 'ECHOOFF', 'ECHOON',
                             'EIGB', 'EIGC', 'EIGP', 'EIGR', 'EIGRL', 'ELIST', 'EOSPOL',
                             'EPOINT', 'EXTRN', 'FEEDGE', 'FEFACE', 'FLFACT', 'FLSYM', 'FLUTTER',
                             'FORCE', 'FORCE1', 'FORCE2', 'FORCEAX', 'FREEPT', 'FREQ', 'FREQ1', 'FREQ2',
                             'FREQ3', 'FREQ4', 'FREQ5', 'FSLIST', 'GENEL', 'GMBC', 'GMBNDC', 'GMBNDS',
                             'GMCONV', 'GMCORD', 'GMCURV', 'GMINTC', 'GMINTS', 'GMLOAD', 'GMNURB',
                             'GMQVOL', 'GMSPC', 'GMSURF', 'GRAV', 'GRDSET', 'GRID', 'GRIDB', 'GRIDF',
                             'GRIDS', 'GUST', 'INCLUDE', 'IPSTRAIN', 'ISTRESS', 'ITER', 'JPLAC', 'LOAD',
                             'LOADCYH', 'LOADCYN', 'LOADCYT', 'LSEQ', 'MARCIN', 'MARCOUT', 'MAT1', 'MAT10',
                             'MAT12', 'MAT2', 'MAT3', 'MAT4', 'MAT5', 'MAT8', 'MAT9', 'MATD001', 'MATD003',
                             'MATD005', 'MATD006', 'MATD007', 'MATD009', 'MATD010', 'MATD012', 'MATD013',
                             'MATD014', 'MATD015', 'MATD018', 'MATD019', 'MATD020', 'MATD022', 'MATD024',
                             'MATD026', 'MATD027', 'MATD028', 'MATD030', 'MATD031', 'MATD054', 'MATD057',
                             'MATD059', 'MATD062', 'MATD063', 'MATD064', 'MATD077', 'MATD080', 'MATD081',
                             'MATD100', 'MATD127', 'MATD181', 'MATD20M', 'MATD2AN', 'MATD2OR', 'MATDERO',
                             'MATEP', 'MATF', 'MATG', 'MATHE', 'MATHED', 'MATHP', 'MATORT', 'MATS1',
                             'MATT1', 'MATT2', 'MATT3', 'MATT4', 'MATT5', 'MATT8', 'MATT9', 'MATTEP',
                             'MATTG', 'MATTHE', 'MATTORT', 'MATTVE', 'MATVE', 'MATVP', 'MBAR', 'MBOLT',
                             'MBOLTUS', 'MDMIOUT', 'MFLUID', 'MKAERO1', 'MKAERO2', 'MODTRAK', 'MOMAX',
                             'MOMENT', 'MOMENT1', 'MOMENT2', 'MONPNT1', 'MPC', 'MPCADD', 'MPCAX',
                             'MPLATE', 'MPROCS', 'MSTACK', 'NLAUTO', 'NLDAMP', 'NLPARM', 'NLPCI',
                             'NLRGAP', 'NLRSFD', 'NLSTEP', 'NLSTRAT', 'NOLIN1', 'NOLIN2', 'NOLIN3',
                             'NOLIN4', 'NSM', 'NSM1', 'NSMADD', 'NSML', 'NSML1', 'NTHICK', 'OMIT',
                             'OMIT1', 'OMITAX', 'OUTPUT', 'OUTRCV', 'PAABSF', 'PACABS', 'PACBAR', 'PACINF',
                             'PACOUS', 'PAERO1', 'PAERO2', 'PAERO3', 'PAERO4', 'PAERO5', 'PANEL',
                             'PARAMARC', 'PBAR', 'PBARL', 'PBCOMP', 'PBEAM', 'PBEAML', 'PBEND',
                             'PBMSECT', 'PBRSECT', 'PBUSH', 'PBUSH1D', 'PBUSHT', 'PCOMP', 'PCOMPG',
                             'PCONEAX', 'PCONV', 'PCONVM', 'PDAMP', 'PDAMP5', 'PDAMPT', 'PDUMi', 'PELAS',
                             'PELAST', 'PEULER1', 'PGAP', 'PHBDY', 'PINTC', 'PINTS', 'PJOINT', 'PLATE',
                             'PLOAD', 'PLOAD1', 'PLOAD2', 'PLOAD4', 'PLOADX1', 'PLOTEL', 'PLPLANE',
                             'PLSOLID', 'PMASS', 'POINT', 'POINTAX', 'PRAC2D', 'PRAC3D', 'PRESAX',
                             'PRESPT', 'PROD', 'PSET', 'PSHEAR', 'PSHELL', 'PSHELL1', 'PSHELLD', 'PSHLN1',
                              'PSLDN1', 'PSOLID', 'PSOLIDD', 'PTUBE', 'PVAL', 'PVISC', 'PWAVE', 'PWELD',
                              'QBDY1', 'QBDY2', 'QBDY3', 'QHBDY', 'QSET', 'QSET1', 'QVECT', 'QVOL',
                              'RADBC', 'RADBND', 'RADCAV', 'RADLST', 'RADM', 'RADMT', 'RADMTX', 'RADSET',
                              'RANDPS', 'RANDT1', 'RBAR', 'RBAR1', 'RBE1', 'RBE2', 'RBE3', 'RBE3D', 'RCROSS',
                              'RELEASE', 'RESTART', 'RFORCE', 'RGYRO', 'RINGAX', 'RINGFL', 'RJOINT', 'RLOAD1',
                              'RLOAD2', 'ROTORG', 'RROD', 'RSPINR', 'RSPINT', 'RSPLINE', 'RSSCON', 'RTRPLT',
                              'RTRPLT1', 'RVDOF', 'RVDOF1', 'SEBNDRY', 'SEBSET', 'SEBSET1', 'SEBULK',
                              'SECONCT', 'SECSET', 'SECSET1', 'SECTAX', 'SEELT', 'SEEXCLD', 'SELABEL',
                              'SELOC', 'SEMPLN', 'SENQSET', 'SEQGP', 'SEQSEP', 'SEQSET', 'SEQSET1',
                              'SESET', 'SESUP', 'SET1', 'SET2', 'SET3', 'SETREE', 'SEUSET', 'SEUSET1',
                              'SLBDY', 'SLOAD', 'SPC', 'SPC1', 'SPCADD', 'SPCAX', 'SPCD', 'SPCOFF',
                              'SPCOFF1', 'SPLINE1', 'SPLINE2', 'SPLINE3', 'SPLINE4', 'SPLINE5', 'SPOINT',
                              'SUPAX', 'SUPORT', 'SUPORT1', 'SWLDPRM', 'TABDMP1', 'TABLE3D', 'TABLED1',
                              'TABLED2', 'TABLED3', 'TABLED4', 'TABLEM1', 'TABLEM2', 'TABLEM3', 'TABLEM4',
                              'TABLES1', 'TABLEST', 'TABRND1', 'TABRNDG', 'TEMP', 'TEMPAX', 'TEMPBC', 'TEMPD',
                              'TEMPF', 'TEMPP1', 'TEMPP3', 'TEMPRB', 'TF', 'TIC', 'TIC3', 'TICD', 'TLOAD1',
                              'TLOAD2', 'TMPSET', 'TOPVAR', 'TRIM', 'TSTEP', 'TSTEPNL', 'TTEMP', 'UNBALNC',
                              'USET', 'USET1', 'UXVEC', 'VIEW', 'VIEW3D', 'WALL']
        QSyntaxHighlighter.__init__( self, parent )



    def highlightBlock(self, text):
        myFormat = QTextCharFormat()

        text = str(text)

        if text:
            fields, notation = cardreader.getFields(text)

            if fields[0].replace('*', '') in self.nastranCards:
                myFormat.setForeground(Qt.darkGreen)
                self.setFormat(0, len(fields[0]), myFormat)
            elif text.strip() in self.delimiters :
                myFormat.setForeground(Qt.red)
                self.setFormat(0, len(text), myFormat)
            elif text.split() and ( text.split()[0].upper() == 'NASTRAN' or text.split()[0].upper() == 'SOL' ) :
                myFormat.setForeground(Qt.red)
                self.setFormat(0, len(text), myFormat)
            elif text.strip().startswith('$'):
                myFormat.setForeground(Qt.darkGray)
                self.setFormat(0, len(text), myFormat)

            if len(text) > 72:
                myFormat.setForeground(Qt.darkGray)
                self.setFormat(72, len(text)-72, myFormat)

